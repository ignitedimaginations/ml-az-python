# Natural Language Processing

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Importing the dataset (# quoting = 3 ignores the double quotes)
dataset = pd.read_csv("../../data/part-7-nlp/Restaurant_Reviews.tsv", delimiter='\t', quoting=3)

# Cleaning the texts
import re
import nltk
# Stopwords is list of irrelevant words like this that etc
# https://stackoverflow.com/questions/41348621/ssl-error-downloading-nltk-data
nltk.download('stopwords')
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
# Clean the reviews and add to list corpus
corpus = []
for i in range(0, 1000):
    # Remove every word which not small or caps a-z
    review = re.sub('[^a-zA-Z]', ' ', dataset['Review'][i])
    # convert review to lower case
    review = review.lower()
    # Tokenize and convert into list of words
    review = review.split()

    # Stemming - keep the root of any word - convert Liked to Like
    ps = PorterStemmer()
    review = [ps.stem(word) for word in review if not word in set(stopwords.words('english'))]
    # Join back cleaned words into a statement
    review = ' '.join(review)
    corpus.append(review)

# Creating the Bag of Words model
from sklearn.feature_extraction.text import CountVectorizer
cv = CountVectorizer(max_features=1500)
x = cv.fit_transform(corpus).toarray()
y = dataset.iloc[:, 1].values

# Use Naive Bayes classification on Bag of Words model

# SPLITTING THE DATASET INTO TRAINING SET AND TEST SET
from sklearn.model_selection import train_test_split
# Dividing data set into 80% Training and 20% Test data, set test_size = 0.2, set random state as 0 for no randomness
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.20, random_state=0)
# Let's check the training sets and test sets
x_train, x_test, y_train, y_test


# Fitting Naive Bayes to our training set
from sklearn.naive_bayes import GaussianNB
classifier = GaussianNB()
classifier.fit(x_train, y_train)

# Predicting the Test set results
y_pred = classifier.predict(x_test)

# Making the confusion matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)




