# Decision Tree Regression

# Data Preprocessing

# IMPORTING THE LIBRARIES
# Mathematical tools
# To ploy diagrams
# To import and manage datasets
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# IMPORTING THE DATASET


dataset = pd.read_csv("../../../data/part-2-regression/section-8-decision-tree-regression/Position_Salaries.csv")
x = dataset.iloc[:, 1:2].values
y = dataset.iloc[:, 2].values



# Fitting Decision Tree model to dataset
from sklearn.tree import DecisionTreeRegressor
# Create your regressor here - DecisionTreeRegressor
# We are going to use criterion as mse - Mean Squared Error (default)
regressor = DecisionTreeRegressor(random_state=0)
regressor.fit(x, y)


# Predicting a new result with Regression
y_pred = regressor.predict(([[6.5]]))


# Visualizing the Regression results
plt.scatter(x, y, color='red')
plt.plot(x, regressor.predict(x), color='blue')
plt.title('Truth or Bluff (Decision Tree Regression Model)')
plt.xlabel('Position Level')
plt.ylabel('Salary')
plt.show()

# Visualizing the Regression results (For higher resolution and smoother curve)
x_grid = np.arange(min(x), max(x), 0.01)
x_grid = x_grid.reshape((len(x_grid), 1))
plt.scatter(x, y, color='red')
plt.plot(x_grid, regressor.predict(x_grid), color='blue')
plt.title('Truth or Bluff (Decision Tree Regression Model)')
plt.xlabel('Position Level')
plt.ylabel('Salary')
plt.show()









