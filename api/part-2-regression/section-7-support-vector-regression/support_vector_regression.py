#SVR

# Data Preprocessing

# IMPORTING THE LIBRARIES
# Mathematical tools
# To ploy diagrams
# To import and manage datasets
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# IMPORTING THE DATASET


dataset = pd.read_csv("../../../data/part-2-regression/section-7-support-vector-regression/Position_Salaries.csv")
x = dataset.iloc[:, 1:2].values
y = dataset.iloc[:, 2].values

# We need to apply feature scaling here as it's not being automatically taken care of by SVR, unlike linear regression
# Feature Scaling
from sklearn.preprocessing import StandardScaler
sc_x = StandardScaler()
sc_y = StandardScaler()
x = sc_x.fit_transform(x)

y = y.reshape((len(y), 1))
y = sc_y.fit_transform(y)


# Fitting SVR model to dataset
from sklearn.svm import SVR
# Create your regressor here - Any regressor - LinearRegression used for compilation
regressor = SVR(kernel='rbf')
regressor.fit(x, y)


# Predicting a new result with SVR
y_pred_scaled = regressor.predict(sc_x.transform(np.array([[6.5]])))
y_pred = sc_y.inverse_transform(y_pred_scaled)


# Visualizing the SVR results
plt.scatter(x, y, color='red')
plt.plot(x, regressor.predict(x), color='blue')
plt.title('Truth or Bluff (SVR)')
plt.xlabel('Position Level')
plt.ylabel('Salary')
plt.show()

# Visualizing the SVR results (For higher resolution and smoother curve)
x_grid = np.arange(min(x), max(x), 0.1)
x_grid = x_grid.reshape((len(x_grid), 1))
plt.scatter(x, y, color='red')
plt.plot(x_grid, regressor.predict(x_grid), color='blue')
plt.title('Truth or Bluff (SVR)')
plt.xlabel('Position Level')
plt.ylabel('Salary')
plt.show()









