# Simple Linear Regression

# Data Preprocessing

# IMPORTING THE LIBRARIES
# Mathematical tools
import numpy as np
# To ploy diagrams
import matplotlib.pyplot as plt
# To import and manage datasets
import pandas as pd

# IMPORTING THE DATASET
from sklearn.impute import SimpleImputer

dataset = pd.read_csv("../../../data/part-2-regression/section-4-simple-linear-regression/salary_data.csv")
x = dataset.iloc[:, :-1].values
y = dataset.iloc[:, 1].values


# SPLITTING THE DATASET INTO TRAINING SET AND TEST SET
from sklearn.model_selection import train_test_split
# Dividing data set into 80% Training and 20% Test data, set test_size = 0.2, set random state as 0 for no randomness
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=1/3, random_state=0)
# Let's check the training sets and test sets
x_train, x_test, y_train, y_test

# Fitting Simple Linear Regression to the Training set
from sklearn.linear_model import LinearRegression
regressor = LinearRegression()
# Supply the training data to the linear regressor
regressor.fit(x_train, y_train)

# Predicting the Test set results
y_pred = regressor.predict(x_test)

# Visualising the Training set results
plt.scatter(x_train, y_train, color='red')
plt.plot(x_train, regressor.predict(x_train), color='blue')
plt.title('Salary vs Experience (Training Set)')
plt.xlabel('Years of experience')
plt.ylabel('Salary')
plt.show()

# Visualising the Test set results
plt.scatter(x_test, y_test, color='red')
plt.plot(x_train, regressor.predict(x_train), color='blue')
plt.title('Salary vs Experience (Test Set)')
plt.xlabel('Years of experience')
plt.ylabel('Salary')
plt.show()