# Data Preprocessing

# IMPORTING THE LIBRARIES
# Mathematical tools
# To ploy diagrams
# To import and manage datasets
import numpy as np
import pandas as pd

# IMPORTING THE DATASET


dataset = pd.read_csv("../../../data/part-2-regression/section-5-multiple-linear-regression/50_Startups.csv")
x = dataset.iloc[:, :-1].values
y = dataset.iloc[:, 4].values


# CATEGORICAL ENCODING FOR CITY COLUMN
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
# Create numerical values for State column using LabelEncoder
# labelEncoder_x = LabelEncoder()
# x[:, 3] = labelEncoder_x.fit_transform(x[:, 3])
# Create n dummy variables for n categories using oneHotEncoder
from sklearn.compose import ColumnTransformer
# columnTransformer = ColumnTransformer(x[:, 3])
# oneHotEncoder = OneHotEncoder(categorical_features=[3])

ct = ColumnTransformer(
    [('one_hot_encoder', OneHotEncoder(), [3])],    # The column numbers to be transformed (here is [0] but can be [0, 1, 3])
    remainder='passthrough'                         # Leave the rest of the columns untouched
)

x = np.array(ct.fit_transform(x), dtype=np.float)
# x = columnTransformer.fit_transform(X=x).toarray()

# Avoiding the dummy variable trap - use only n-1 dummy variables
# Python library for linear regression takes care of this, we don't need to do this manually
# Let's remove the first column of dummy variables -> [:, 1:]
x = x[:, 1:]

# SPLITTING THE DATASET INTO TRAINING SET AND TEST SET
from sklearn.model_selection import train_test_split

# Dividing data set into 80% Training and 20% Test data, set test_size = 0.2, set random state as 0 for no randomness
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=0)
# Let's check the training sets and test sets
x_train, x_test, y_train, y_test

# No need to add feature scaling code as the library takes care of it

# Fitting Multiple Linear Regression to the training set
from sklearn.linear_model import LinearRegression
regressor = LinearRegression()
# Supply the training data to the linear regressor
regressor.fit(x_train, y_train)

# Predicting the Test Set results
y_pred = regressor.predict(x_test)

# Building the optimal model using Backward Elimination
import statsmodels.regression.linear_model as sm


x = np.append(arr=np.ones((50, 1)).astype(int), values=x, axis=1)
x_opt = x[:, [0, 1, 2, 3, 4, 5]]
# Ordinary Least Squares regressor - A multiple linear regression algorithm
regressor_ols = sm.OLS(endog=y, exog=x_opt).fit()
regressor_ols.summary()

# Next step - remove x2 as it has a p value of 99%
x_opt = x[:, [0, 1, 3, 4, 5]]
# Ordinary Least Squares regressor - A multiple linear regression algorithm
regressor_ols = sm.OLS(endog=y, exog=x_opt).fit()
regressor_ols.summary()

# Next step - remove x1 as it has a p value of 93%
x_opt = x[:, [0, 3, 4, 5]]
# Ordinary Least Squares regressor - A multiple linear regression algorithm
regressor_ols = sm.OLS(endog=y, exog=x_opt).fit()
regressor_ols.summary()

# Next step - remove x4 as it has a p value of 60%
x_opt = x[:, [0, 3, 5]]
# Ordinary Least Squares regressor - A multiple linear regression algorithm
regressor_ols = sm.OLS(endog=y, exog=x_opt).fit()
regressor_ols.summary()

# Next step - x5 has 6% as p value. a bit above 5%.
# Remove x5, and we see that x3 is the only impacting column and that's R&D spend
x_opt = x[:, [0, 3]]
# Ordinary Least Squares regressor - A multiple linear regression algorithm
regressor_ols = sm.OLS(endog=y, exog=x_opt).fit()
regressor_ols.summary()


# AUTOMATIC IMPLEMENTATIONS OF BACKWARD ELIMINATION
# Backward Elimination with p values only
def backwardEliminationPvalue(x, sl):
    numVars = len(x[0])
    for i in range(0, numVars):
        regressor_ols = sm.OLS(endog=y, exog=x).fit()
        maxVar = max(regressor_ols.pvalues).astype(float)
        if maxVar > sl:
            for j in range(0, numVars - i):
                if (regressor_ols.pvalues[j].astype(float) == maxVar):
                    x = np.delete(x, j, 1)
    regressor_ols.summary()
    return x

sl = 0.05
x_opt = x[:, [0, 1, 2, 3, 4, 5]]
x_modeled = backwardEliminationPvalue(x_opt, sl)


# Backward Elimination with p values nad adjuest r squared
def backwardEliminationPvalueAdjRSquared(x, sl):
    numVars = len(x[0])
    temp = np.zeros((50,6)).astype(int)
    for i in range(0, numVars):
        regressor_ols = sm.OLS(y, x).fit()
        maxVar = max(regressor_ols.pvalues).astype(float)
        adjR_before = regressor_ols.rsquared_adj.astype(float)
        if maxVar > sl:
            for j in range(0, numVars - i):
                if(regressor_ols.pvalues[j].astype(float) == maxVar):
                    temp[:, j] = x[:, j]
                    x = np.delete(x, j, 1)
                    tmp_regressor = sm.OLS(y, x).fit()
                    adjR_after = tmp_regressor.rsquared_adj.astype(float)
                    if (adjR_before >= adjR_after):
                        x_rollback = np.hstack((x, temp[:, [0, j]]))
                        x_rollback = np.delete(x_rollback, j, 1)
                        print(regressor_ols.summary())
                        return x_rollback
                    else:
                        continue
    regressor_ols.summary()
    return x


sl = 0.05
x_opt = x[:, [0, 1, 2, 3, 4, 5]]
x_modeled = backwardEliminationPvalueAdjRSquared(x_opt, sl)










