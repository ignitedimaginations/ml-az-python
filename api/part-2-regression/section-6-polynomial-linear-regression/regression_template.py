# Regression Template - To build regression models. Make predictions.

# Data Preprocessing

# IMPORTING THE LIBRARIES
# Mathematical tools
# To ploy diagrams
# To import and manage datasets
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# IMPORTING THE DATASET


dataset = pd.read_csv("../../../data/part-2-regression/section-6-polynomial-linear-regression/Position_Salaries.csv")
x = dataset.iloc[:, 1:2].values
y = dataset.iloc[:, 2].values

# No need to do encoding as we don't need the position name, level is already encoded and associated with it
# No need to create a test and training set split as we have only 10 entries


# Fitting Regression model to dataset
from sklearn.linear_model import LinearRegression
# Create your regressor here - Any regressor - LinearRegression used for compilation
regressor = LinearRegression()


# Predicting a new result with Regression
y_pred = regressor.predict(([[6.5]]))


# Visualizing the Regression results
plt.scatter(x, y, color='red')
plt.plot(x, regressor.predict(x), color='blue')
plt.title('Truth or Bluff (Regression Model)')
plt.xlabel('Position Level')
plt.ylabel('Salary')
plt.show()

# Visualizing the Regression results (For higher resolution and smoother curve)
x_grid = np.arange(min(x), max(x), 0.1)
x_grid = x_grid.reshape((len(x_grid), 1))
plt.scatter(x, y, color='red')
plt.plot(x_grid, regressor.predict(x_grid), color='blue')
plt.title('Truth or Bluff (Regression Model)')
plt.xlabel('Position Level')
plt.ylabel('Salary')
plt.show()








