# Data Preprocessing

# IMPORTING THE LIBRARIES
# Mathematical tools
# To ploy diagrams
# To import and manage datasets
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# IMPORTING THE DATASET


dataset = pd.read_csv("../../../data/part-2-regression/section-6-polynomial-linear-regression/Position_Salaries.csv")
x = dataset.iloc[:, 1:2].values
y = dataset.iloc[:, 2].values

# No need to do encoding as we don't need the position name, level is already encoded and associated with it
# No need to create a test and training set split as we have only 10 entries

# Fitting linear regression to dataset
from sklearn.linear_model import  LinearRegression
lin_reg = LinearRegression()
lin_reg.fit(x, y)

# Fitting polynominal regression to dataset
from sklearn.preprocessing import PolynomialFeatures
poly_reg = PolynomialFeatures(degree=4)
# Notice that the constant b0 (as 1s) has been automatically added in the 1st column of x_poly
x_poly = poly_reg.fit_transform(x)

# Now we need to include the above fit fo polynomial features ina linear regression model
lin_reg_2 = LinearRegression()
lin_reg_2.fit(x_poly, y)

# Visualizing the Linear Regression results
plt.scatter(x, y, color='red')
plt.plot(x, lin_reg.predict(x), color='blue')
plt.title('Truth or Bluff (Linear Regression)')
plt.xlabel('Position Level')
plt.ylabel('Salary')
plt.show()


# Visualizing the Polynomial Regression results
x_grid = np.arange(min(x), max(x), 0.1)
x_grid = x_grid.reshape((len(x_grid), 1))
plt.scatter(x, y, color='red')
plt.plot(x_grid, lin_reg_2.predict(poly_reg.fit_transform(x_grid)), color='blue')
plt.title('Truth or Bluff (Polynomial Regression)')
plt.xlabel('Position Level')
plt.ylabel('Salary')
plt.show()


# Predicting a new result with Linear Regression
lin_reg_pred_salary = lin_reg.predict([[6.5]])

# Predicting a new result with Polynomial Regression
poly_reg_pred_salary = lin_reg_2.predict(poly_reg.fit_transform([[6.5]]))








