# Upper Confidence Bound

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import math as mt

# Importing the dataset
dataset = pd.read_csv("../../../data/part-6-reinforcement-learning/section-27-ucb/Ads_CTR_Optimisation.csv")

# Implementing UCB

N = 10000
d = 10
# List of ads selected in each round
ads_selected = []
# Create array of 0s
numbers_of_selections = [0] * d
sums_of_rewards = [0] * d
total_reward = 0
for n in range(0, N):
    # This gets initialized in each round
    ad = 0
    max_upper_bound = 0
    for i in range(0, d):
        # Don't do this for 1st 10 rounds, to fetch some initial values
        if (numbers_of_selections[i] > 0):
            # For each version of ad calculate avg reward and confidence interval
            average_reward = sums_of_rewards[i] / numbers_of_selections[i]
            # Calculate Delta [i]
            delta_i = mt.sqrt(3 / 2 * mt.log(n + 1) / numbers_of_selections[i])
            # Calculate confidence interval. This is step 2.
            upper_bound = average_reward + delta_i
        else:
            upper_bound = 1e400
        if upper_bound > max_upper_bound:
            max_upper_bound = upper_bound
            # Select the ad version that has max upper bound
            ad = i
    ads_selected.append(ad)
    numbers_of_selections[ad] = numbers_of_selections[ad] + 1
    reward = dataset.values[n, ad]
    sums_of_rewards[ad] = sums_of_rewards[ad] + reward
    total_reward = total_reward + reward

# Visualizing the results
plt.hist(ads_selected)
plt.title('Histogram of ads selections')
plt.xlabel('Ads')
plt.ylabel('Number of times each ad was selected')
plt.show()
