# Thompson Sampling

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import math as mt
import random as rd

# Importing the dataset
dataset = pd.read_csv("../../../data/part-6-reinforcement-learning/section-28-thompson-sampling/Ads_CTR_Optimisation.csv")

# Implementing Thompson Sampling

N = 10000
d = 10
# List of ads selected in each round
ads_selected = []
# Create vector of 10 0s
numbers_of_rewards_1 = [0] * d
numbers_of_rewards_0 = [0] * d
total_reward = 0
for n in range(0, N):
    # This gets initialized in each round
    ad = 0
    max_random = 0
    for i in range(0, d):
        # Generate random draws for each of 10 ads, using random - Beta distribution function
        random_beta = rd.betavariate(numbers_of_rewards_1[i] + 1, numbers_of_rewards_0[i] + 1)
        if random_beta > max_random:
            max_random = random_beta
            # Select the ad version that has max random distribution
            ad = i
    ads_selected.append(ad)
    reward = dataset.values[n, ad]
    # Adjust the distribution as per the actual reward
    if reward == 1:
        numbers_of_rewards_1[ad] = numbers_of_rewards_1[ad] + 1
    else:
        numbers_of_rewards_0[ad] = numbers_of_rewards_0[ad] + 1
    total_reward = total_reward + reward

# Visualizing the results
plt.hist(ads_selected)
plt.title('Histogram of ads selections')
plt.xlabel('Ads')
plt.ylabel('Number of times each ad was selected')
plt.show()
