# Logistic Regression

# Data Preprocessing

# IMPORTING THE LIBRARIES
# Mathematical tools
# To ploy diagrams
# To import and manage datasets
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
#import requests

# IMPORTING THE DATASET


dataset = pd.read_csv("../../../data/part-3-classification/section-12-logistic-regression/good-bad-food.csv")
#url = "https://storage.cloud.google.com/health-a-thon-data/good-bad-food.csv"
#s = requests.get(url).text
#dataset = pd.read_csv(s)
# We just want to include Age and Estimated Salary as the independent variables
x = dataset.iloc[:, 3:35].values
y = dataset.iloc[:, 1].values

# SPLITTING THE DATASET INTO TRAINING SET AND TEST SET
from sklearn.model_selection import train_test_split
# Dividing data set into 80% Training and 20% Test data, set test_size = 0.2, set random state as 0 for no randomness
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.25, random_state=0)
# Let's check the training sets and test sets
x_train, x_test, y_train, y_test

# FEATURE SCALING
# In the given dataset, we have numerical values for age and salary
# Since the scale of salary is much higher than age, in ML equations, using it directly will give a bias to salary
# In order to given age it's due importance we need to scale different variables
# Scaling can be done using methods like Standardisation and Normalisation
# COMMENTED as most of the ML libraries have in-built feature scaling
from sklearn.preprocessing import StandardScaler
sc_x = StandardScaler()
x_train = sc_x.fit_transform(x_train)
x_test = sc_x.fit_transform(x_test)

# Fitting Logistic Regression to our training set
from sklearn.linear_model import LogisticRegression
classifier = LogisticRegression(random_state=0)
classifier.fit(x_train, y_train)

# Predicting the Test set results
y_pred = classifier.predict(x_test)

# Making the confusion matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)

from matplotlib.colors import ListedColormap
