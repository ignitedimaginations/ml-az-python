from operator import itemgetter

from graphframes import *
from graphframes.lib import AggregateMessages as AM
from pyspark.shell import spark, sc
from pyspark.sql import functions as F
from pyspark.sql.types import *

# // tag::udfs[]
add_path_udf = F.udf(lambda path, id: path + [id], ArrayType(StringType()))
# // end::udfs[]

# // tag::via[]
via_udf = F.udf(lambda path: path[1:-1], ArrayType(StringType()))
# // end::via[]

# BFS

node_fields = [
        StructField("id", StringType(), True),
        StructField("latitude", FloatType(), True),
        StructField("longitude", FloatType(), True),
        StructField("population", IntegerType(), True)
    ]
nodes = spark.read.csv("../../data/part-x-extras/transport-nodes.csv", header=True,
                       schema=StructType(node_fields))

rels = spark.read.csv("../../data/part-x-extras/transport-relationships.csv", header=True)
reversed_rels = (rels.withColumn("newSrc", rels.dst)
                 .withColumn("newDst", rels.src)
                 .drop("dst", "src")
                 .withColumnRenamed("newSrc", "src")
                 .withColumnRenamed("newDst", "dst")
                 .select("src", "dst", "relationship", "cost"))

relationships = rels.union(reversed_rels)

graphframes = GraphFrame(nodes, relationships)

from_expr = "id='Den Haag'"
to_expr = "population > 100000 and population < 300000 and id <> 'Den Haag'"
result = graphframes.bfs(from_expr, to_expr)
print(result.columns)
columns = [column for column in result.columns if not column.startswith("e")]
result.select(columns).show()