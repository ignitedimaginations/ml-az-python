from operator import itemgetter

from graphframes import *
from graphframes.lib import AggregateMessages as AM
from pyspark.shell import spark, sc
from pyspark.sql import functions as F
from pyspark.sql.types import *

# // tag::udfs[]
add_path_udf = F.udf(lambda path, id: path + [id], ArrayType(StringType()))
# // end::udfs[]

# // tag::via[]
via_udf = F.udf(lambda path: path[1:-1], ArrayType(StringType()))
# // end::via[]




#Breadth First Search
#Breadth First Search (BFS) is one of the fundamental graph traversal algorithms.
# It starts from a chosen node and explores all of its neighbors at one hop away
# before visiting all the neighbors at two hops away, and so on.
def bfs():
    graphframes = create_transport_graph()

    from_expr = "id='Den Haag'"
    to_expr = "population > 100000 and population < 300000 and id <> 'Den Haag'"
    result = graphframes.bfs(from_expr, to_expr)
    print(result.columns)
    columns = [column for column in result.columns if not column.startswith("e")]
    result.select(columns).show()

#Shortest Path
#The Shortest Path algorithm calculates the shortest (weighted) path between a pair of nodes.
# It’s useful for user interactions and dynamic workflows because it works in real time.
# Use Shortest Path to find optimal routes between a pair of nodes,
# based on either the number of hops or any weighted relationship value.
# For example, it can provide real- time answers about degrees of separation,
# the shortest distance between points, or the least expensive route.
# You can also use this algorithm to simply explore the connections between particular nodes.
def shortestpath():
    graphframes = create_transport_graph()
    result = shortest_path(graphframes, "Amsterdam", "Colchester", "cost")
    result.select("id", "distance", "path").show(truncate=False)


# // tag::custom-shortest-path[]
def shortest_path(g, origin, destination, column_name="cost"):
    if g.vertices.filter(g.vertices.id == destination).count() == 0:
        return (spark.createDataFrame(sc.emptyRDD(), g.vertices.schema)
                .withColumn("path", F.array()))

    vertices = (g.vertices.withColumn("visited", F.lit(False))
                .withColumn("distance", F.when(g.vertices["id"] == origin, 0)
                            .otherwise(float("inf")))
                .withColumn("path", F.array()))
    cached_vertices = AM.getCachedDataFrame(vertices)
    g2 = GraphFrame(cached_vertices, g.edges)

    while g2.vertices.filter('visited == False').first():
        current_node_id = g2.vertices.filter('visited == False').sort("distance").first().id

        msg_distance = AM.edge[column_name] + AM.src['distance']
        msg_path = add_path_udf(AM.src["path"], AM.src["id"])
        msg_for_dst = F.when(AM.src['id'] == current_node_id, F.struct(msg_distance, msg_path))
        new_distances = g2.aggregateMessages(F.min(AM.msg).alias("aggMess"),
                                             sendToDst=msg_for_dst)

        new_visited_col = F.when(
            g2.vertices.visited | (g2.vertices.id == current_node_id), True).otherwise(False)
        new_distance_col = F.when(new_distances["aggMess"].isNotNull() &
                                  (new_distances.aggMess["col1"] < g2.vertices.distance),
                                  new_distances.aggMess["col1"]) \
            .otherwise(g2.vertices.distance)
        new_path_col = F.when(new_distances["aggMess"].isNotNull() &
                              (new_distances.aggMess["col1"] < g2.vertices.distance),
                              new_distances.aggMess["col2"].cast("array<string>")) \
            .otherwise(g2.vertices.path)

        new_vertices = (g2.vertices.join(new_distances, on="id", how="left_outer")
                        .drop(new_distances["id"])
                        .withColumn("visited", new_visited_col)
                        .withColumn("newDistance", new_distance_col)
                        .withColumn("newPath", new_path_col)
                        .drop("aggMess", "distance", "path")
                        .withColumnRenamed('newDistance', 'distance')
                        .withColumnRenamed('newPath', 'path'))
        cached_new_vertices = AM.getCachedDataFrame(new_vertices)
        g2 = GraphFrame(cached_new_vertices, g2.edges)
        if g2.vertices.filter(g2.vertices.id == destination).first().visited:
            return (g2.vertices.filter(g2.vertices.id == destination)
                    .withColumn("newPath", add_path_udf("path", "id"))
                    .drop("visited", "path")
                    .withColumnRenamed("newPath", "path"))
    return (spark.createDataFrame(sc.emptyRDD(), g.vertices.schema)
            .withColumn("path", F.array()))
# // end::custom-shortest-path[]


#All Pairs Shortest Path
#The All Pairs Shortest Path (APSP) algorithm calculates the shortest (weighted) path between all pairs of nodes.
# It’s more efficient than running the Single Source Shortest Path algorithm for every pair of nodes in the graph.
# APSP optimizes operations by keeping track of the distances calculated so far and running on nodes in parallel.
# Those known distances can then be reused when calculating the shortest path to an unseen node.
# You can follow the example in the next section to get a better understanding of how the algorithm works.

def apsp():
    graphframes = create_transport_graph()
    result = graphframes.shortestPaths(["Colchester", "Immingham", "Hoek van Holland"])
    result.sort(["id"]).select("id", "distances").show(truncate=False)

#Single Source Shortest Path
# The Single Source Shortest Path (SSSP) algorithm,
# which came into prominence at around the same time as Dijkstra’s Shortest Path algorithm,
# acts as an implementation for both problems.
# The SSSP algorithm calculates the shortest (weighted) path from a root node to all other nodes in the graph

def sssp():
    graphframes = create_transport_graph()
    result = custom_sssp(graphframes, "Amsterdam", "cost")
    (result
     .withColumn("via", via_udf("path"))
     .select("id", "distance", "via")
     .sort("distance")
     .show(truncate=False))


# // tag::sssp[]
def custom_sssp(g, origin, column_name="cost"):
    vertices = g.vertices \
        .withColumn("visited", F.lit(False)) \
        .withColumn("distance",
            F.when(g.vertices["id"] == origin, 0).otherwise(float("inf"))) \
        .withColumn("path", F.array())
    cached_vertices = AM.getCachedDataFrame(vertices)
    g2 = GraphFrame(cached_vertices, g.edges)

    while g2.vertices.filter('visited == False').first():
        current_node_id = g2.vertices.filter('visited == False').sort("distance").first().id

        msg_distance = AM.edge[column_name] + AM.src['distance']
        msg_path = add_path_udf(AM.src["path"], AM.src["id"])
        msg_for_dst = F.when(AM.src['id'] == current_node_id, F.struct(msg_distance, msg_path))
        new_distances = g2.aggregateMessages(
            F.min(AM.msg).alias("aggMess"), sendToDst=msg_for_dst)

        new_visited_col = F.when(
            g2.vertices.visited | (g2.vertices.id == current_node_id), True).otherwise(False)
        new_distance_col = F.when(new_distances["aggMess"].isNotNull() &
                                  (new_distances.aggMess["col1"] < g2.vertices.distance),
                                  new_distances.aggMess["col1"]) \
                            .otherwise(g2.vertices.distance)
        new_path_col = F.when(new_distances["aggMess"].isNotNull() &
                              (new_distances.aggMess["col1"] < g2.vertices.distance),
                              new_distances.aggMess["col2"].cast("array<string>")) \
                        .otherwise(g2.vertices.path)

        new_vertices = g2.vertices.join(new_distances, on="id", how="left_outer") \
            .drop(new_distances["id"]) \
            .withColumn("visited", new_visited_col) \
            .withColumn("newDistance", new_distance_col) \
            .withColumn("newPath", new_path_col) \
            .drop("aggMess", "distance", "path") \
            .withColumnRenamed('newDistance', 'distance') \
            .withColumnRenamed('newPath', 'path')
        cached_new_vertices = AM.getCachedDataFrame(new_vertices)
        g2 = GraphFrame(cached_new_vertices, g2.edges)

    return g2.vertices \
                .withColumn("newPath", add_path_udf("path", "id")) \
                .drop("visited", "path") \
                .withColumnRenamed("newPath", "path")
# // end::sssp[]


#Degree Centrality
#Degree Centrality is the simplest of the algorithms that we’ll cover in this book.
# It counts the number of incoming and outgoing relationships from a node, and is used to find popular nodes in a graph.
#Use Degree Centrality if you’re attempting to analyze influence by looking at the number of incoming and outgoing relationships,
# or find the “popularity” of individ‐ ual nodes.
# It works well when you’re concerned with immediate connectedness or near-term probabilities.
# However, Degree Centrality is also applied to global analysis
# when you want to evaluate the minimum degree, maximum degree, mean degree, and standard deviation across the entire graph.

def degreecentrality():
    graphframes = create_social_media_graph()
    total_degree = graphframes.degrees
    in_degree = graphframes.inDegrees
    out_degree = graphframes.outDegrees

    (total_degree.join(in_degree, "id", how="left")
     .join(out_degree, "id", how="left")
     .fillna(0)
     .sort("inDegree", ascending=False)
     .show())

#Closeness Centrality
#Closeness Centrality is a way of detecting nodes that are able to spread information efficiently through a subgraph.
#The measure of a node’s centrality is its average farness (inverse distance) to all other nodes.
# Nodes with a high closeness score have the shortest distances from all other nodes.
#For each node, the Closeness Centrality algorithm calculates the sum of its distances to all other nodes,
# based on calculating the shortest paths between all pairs of nodes.
# The resulting sum is then inverted to determine the closeness centrality score for that node.
#Apply Closeness Centrality when you need to know which nodes disseminate things the fastest.
# Using weighted relationships can be especially helpful in evaluating inter‐ action speeds in communication and behavioral analyses.
# Check RA Brandes Variant also: This algorithm calculates a betweenness score by sampling nodes and only exploring shortest paths to a certain depth.

def closecentrality():
    graphframes = create_social_media_graph()
    vertices = graphframes.vertices.withColumn("ids", F.array())
    cached_vertices = AM.getCachedDataFrame(vertices)
    g2 = GraphFrame(cached_vertices, graphframes.edges)

    for i in range(0, g2.vertices.count()):
        msg_dst = new_paths_udf(AM.src["ids"], AM.src["id"])
        msg_src = new_paths_udf(AM.dst["ids"], AM.dst["id"])
        agg = g2.aggregateMessages(F.collect_set(AM.msg).alias("agg"),
                                   sendToSrc=msg_src, sendToDst=msg_dst)
        res = agg.withColumn("newIds", flatten_udf("agg")).drop("agg")
        new_vertices = (g2.vertices.join(res, on="id", how="left_outer")
                        .withColumn("mergedIds", merge_paths_udf("ids", "newIds", "id"))
                        .drop("ids", "newIds")
                        .withColumnRenamed("mergedIds", "ids"))
        cached_new_vertices = AM.getCachedDataFrame(new_vertices)
        g2 = GraphFrame(cached_new_vertices, g2.edges)

    (g2.vertices
     .withColumn("closeness", closeness_udf("ids"))
     .sort("closeness", ascending=False)
     .show(truncate=False))


def collect_paths(paths):
    return F.collect_set(paths)


collect_paths_udf = F.udf(collect_paths, ArrayType(StringType()))

paths_type = ArrayType(
    StructType([StructField("id", StringType()), StructField("distance", IntegerType())]))


def flatten(ids):
    flat_list = [item for sublist in ids for item in sublist]
    return list(dict(sorted(flat_list, key=itemgetter(0))).items())

flatten_udf = F.udf(flatten, paths_type)

def new_paths(paths, id):
    paths = [{"id": col1, "distance": col2 + 1} for col1, col2 in paths if col1 != id]
    paths.append({"id": id, "distance": 1})
    return paths


new_paths_udf = F.udf(new_paths, paths_type)

def merge_paths(ids, new_ids, id):
    joined_ids = ids + (new_ids if new_ids else [])
    merged_ids = [(col1, col2) for col1, col2 in joined_ids if col1 != id]
    best_ids = dict(sorted(merged_ids, key=itemgetter(1), reverse=True))
    return [{"id": col1, "distance": col2} for col1, col2 in best_ids.items()]


merge_paths_udf = F.udf(merge_paths, paths_type)

def calculate_closeness(ids):
    nodes = len(ids)
    total_distance = sum([col2 for col1, col2 in ids])
    return 0 if total_distance == 0 else nodes * 1.0 / total_distance


closeness_udf = F.udf(calculate_closeness, DoubleType())


#PageRank
#PageRank is the best known of the centrality algorithms.
# It measures the transitive (or directional) influence of nodes.
# All the other centrality algorithms we discuss meas‐ ure the direct influence of a node,
# whereas PageRank considers the influence of a node’s neighbors, and their neighbors.
# For example, having a few very powerful friends can make you more influential than having a lot of less powerful friends.
# PageRank is computed either by iteratively distributing one node’s rank over its neighbors or by randomly
# traversing the graph and counting the frequency with which each node is hit during these walks.
#PageRank is named after Google cofounder Larry Page, who created it to rank web‐ sites in Google’s search results.
# The basic assumption is that a page with more incom‐ ing and more influential incoming links is more likely a credible source.
# PageRank measures the number and quality of incoming relationships to a node to determine an estimation of how important that node is.
# Nodes with more sway over a network are presumed to have more incoming relationships from other influential nodes.
#GraphFrames supports two implementations of PageRank:
#The first implementation runs PageRank for a fixed number of iterations. This can be run by setting the maxIter parameter.
#The second implementation runs PageRank until convergence. This can be run by setting the tol parameter.

def pagerank():
    graphframes = create_social_media_graph()
    results = graphframes.pageRank(resetProbability=0.15, maxIter=20)
    results.vertices.sort("pagerank", ascending=False).show()

    results = graphframes.pageRank(resetProbability=0.15, tol=0.01)
    results.vertices.sort("pagerank", ascending=False).show()


#PageRank Variation: Personalized PageRank
#Personalized PageRank (PPR) is a variant of the PageRank algorithm that calculates
# the importance of nodes in a graph from the perspective of a specific node.
# For PPR, random jumps refer back to a given set of starting nodes.
# This biases results toward, or personalizes for, the start node.
# This bias and localization make PPR useful for highly targeted recommendations.

def personalizedpagerank():
    graphframes = create_social_media_graph()
    me = "Doug"
    results = graphframes.pageRank(resetProbability=0.15, maxIter=20, sourceId=me)
    people_to_follow = results.vertices.sort("pagerank", ascending=False)

    already_follows = list(graphframes.edges.filter(f"src = '{me}'").toPandas()["dst"])
    people_to_exclude = already_follows + [me]

    people_to_follow[~people_to_follow.id.isin(people_to_exclude)].show()

# Triangle Count and Clustering Coefficient
# The Triangle Count and Clustering Coefficient algorithms are presented together because they are so often used together.
# Triangle Count determines the number of tri‐ angles passing through each node in the graph.
# A triangle is a set of three nodes, where each node has a relationship to all other nodes.
# Triangle Count can also be run globally for evaluating our overall dataset.
# The goal of the Clustering Coefficient algorithm is to measure
# how tightly a group is clustered compared to how tightly it could be clustered.
# The algorithm uses Triangle Count in its calculations, which provides a ratio of existing triangles to possible relationships.
# A maximum value of 1 indicates a clique where every node is connected to every other node.
# There are two types of clustering coefficients: local clustering and global clustering.

def trianglecount():
    graphframes = create_software_graph()
    result = graphframes.triangleCount()
    (result.sort("count", ascending=False)
     .filter('count > 0')
     .show())


# Strongly Connected Components
# The Strongly Connected Components (SCC) algorithm is one of the earliest graph algorithms.
# SCC finds sets of connected nodes in a directed graph where each node is reachable in both directions
# from any other node in the same set. Its runtime operations scale well, proportional to the number of nodes.
# In Figure 6-5 you can see that the nodes in an SCC group don’t need to be immediate neighbors,
# but there must be directional paths between all nodes in the set.

def scc():
    graphframes = create_software_graph()
    result = graphframes.stronglyConnectedComponents(maxIter=10)
    (result.sort("component")
     .groupby("component")
     .agg(F.collect_list("id").alias("libraries"))
     .show(truncate=False))

# Connected Components
# The Connected Components algorithm (sometimes called Union Find or Weakly Connected Components)
# finds sets of connected nodes in an undirected graph where each node is reachable from any other node in the same set.
# It differs from the SCC algorithm because it only needs a path to exist between pairs of nodes in one direction,
# whereas SCC needs a path to exist in both directions.

def cc():
    graphframes = create_software_graph()
    spark.sparkContext.setCheckpointDir('data/checkpoint')
    result = graphframes.connectedComponents()
    (result.sort("component")
     .groupby("component")
     .agg(F.collect_list("id").alias("libraries"))
     .show(truncate=False))

#Label Propagation
#The Label Propagation algorithm (LPA) is a fast algorithm for finding communities in a graph.
# In LPA, nodes select their group based on their direct neighbors.
# This process is well suited to networks where groupings are less clear and weights can be used to help a node determine
# which community to place itself within. It also lends itself well to semisupervised learning
# because you can seed the process with preassigned, indicative node labels.
# The intuition behind this algorithm is that a single label can quickly become dominant
# in a densely connected group of nodes, but it will have trouble crossing a sparsely connected region.
# Labels get trapped inside a densely connected group of nodes, and nodes that end up with the same label
# when the algorithm finishes are considered part of the same community. The algorithm resolves overlaps,
# where nodes are potentially part of multiple clusters, by assigning membership to the label neighborhood
# with the highest combined relationship and node weight.

def labelpropagation():
    graphframes = create_software_graph()
    spark.sparkContext.setCheckpointDir('data/checkpoint')
    result = graphframes.labelPropagation(maxIter=10)
    (result
     .sort("label")
     .groupby("label")
     .agg(F.collect_list("id"))
     .show(truncate=False))


# // tag::load-graph-frame[]
def create_transport_graph():
    node_fields = [
        StructField("id", StringType(), True),
        StructField("latitude", FloatType(), True),
        StructField("longitude", FloatType(), True),
        StructField("population", IntegerType(), True)
    ]
    nodes = spark.read.csv("ml-az-python/data/part-x-extras/transport-nodes.csv", header=True,
                           schema=StructType(node_fields))

    rels = spark.read.csv("ml-az-python/data/part-x-extras/transport-relationships.csv", header=True)
    reversed_rels = (rels.withColumn("newSrc", rels.dst)
                     .withColumn("newDst", rels.src)
                     .drop("dst", "src")
                     .withColumnRenamed("newSrc", "src")
                     .withColumnRenamed("newDst", "dst")
                     .select("src", "dst", "relationship", "cost"))

    relationships = rels.union(reversed_rels)

    return GraphFrame(nodes, relationships)
# // end::load-graph-frame[]

# // tag::load-graph-frame[]
def create_social_media_graph():
    v = spark.read.csv("ml-az-python/data/part-x-extras/social-nodes.csv", header=True)
    e = spark.read.csv("ml-az-python/data/part-x-extras/social-relationships.csv", header=True)
    return GraphFrame(v, e)
# // end::load-graph-frame[]

def create_software_graph():
    nodes = spark.read.csv("ml-az-python/data/part-x-extras/sw-nodes.csv", header=True)
    relationships = spark.read.csv("ml-az-python/data/part-x-extras/sw-relationships.csv", header=True)
    return GraphFrame(nodes, relationships)

def create_airport_graph():
    nodes = spark.read.csv("ml-az-python/data/part-x-extras/airports.csv", header=False)
    cleaned_nodes = (nodes.select("_c1", "_c3", "_c4", "_c6", "_c7")
                     .filter("_c3 = 'United States'")
                     .withColumnRenamed("_c1", "name")
                     .withColumnRenamed("_c4", "id")
                     .withColumnRenamed("_c6", "latitude")
        .withColumnRenamed("_c7", "longitude")
        .drop("_c3"))
    cleaned_nodes = cleaned_nodes[cleaned_nodes["id"] != "\\N"]

    relationships = spark.read.csv("data/984660862_T_ONTIME_REPORTING.csv", header=True)
    cleaned_relationships = (relationships
                             .select("ORIGIN", "DEST", "FL_DATE", "DEP_DELAY", "ARR_DELAY",
                                     "DISTANCE", "TAIL_NUM", "OP_CARRIER_FL_NUM", "CRS_DEP_TIME",
                                     "CRS_ARR_TIME", "OP_UNIQUE_CARRIER")
                             .withColumnRenamed("ORIGIN", "src")
                             .withColumnRenamed("DEST", "dst")
                             .withColumnRenamed("DEP_DELAY", "deptDelay")
                             .withColumnRenamed("ARR_DELAY", "arrDelay")
                             .withColumnRenamed("TAIL_NUM", "tailNumber")
                             .withColumnRenamed("OP_CARRIER_FL_NUM", "flightNumber")
                             .withColumnRenamed("FL_DATE", "date")
                             .withColumnRenamed("CRS_DEP_TIME", "time")
                             .withColumnRenamed("CRS_ARR_TIME", "arrivalTime")
                             .withColumnRenamed("DISTANCE", "distance")
                             .withColumnRenamed("OP_UNIQUE_CARRIER", "airline")
                             .withColumn("deptDelay", F.col("deptDelay").cast(FloatType()))
                             .withColumn("arrDelay", F.col("arrDelay").cast(FloatType()))
                             .withColumn("time", F.col("time").cast(IntegerType()))
                             .withColumn("arrivalTime", F.col("arrivalTime").cast(IntegerType()))
                             )



    return GraphFrame(cleaned_nodes, cleaned_relationships)

def create_airlines_graph():
    airlines_reference = (spark.read.csv("ml-az-python/data/part-x-extras/airlines.csv")
                          .select("_c1", "_c3")
                          .withColumnRenamed("_c1", "name")
                          .withColumnRenamed("_c3", "code"))

    airlines_reference = airlines_reference[airlines_reference["code"] != "null"]
    return airlines_reference