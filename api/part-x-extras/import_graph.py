# // tag::imports[]
import os

from pyspark.shell import spark
from pyspark.sql.types import *
from pyspark.sql import functions as F
from graphframes import *
# // end::imports[]


# // tag::load-graph-frame[]
def create_transport_graph():
    node_fields = [
        StructField("id", StringType(), True),
        StructField("latitude", FloatType(), True),
        StructField("longitude", FloatType(), True),
        StructField("population", IntegerType(), True)
    ]
    nodes = spark.read.csv("finny-intelligence-engine/data/transport-nodes.csv", header=True,
                           schema=StructType(node_fields))

    rels = spark.read.csv("finny-intelligence-engine/data/transport-relationships.csv", header=True)
    reversed_rels = (rels.withColumn("newSrc", rels.dst)
                     .withColumn("newDst", rels.src)
                     .drop("dst", "src")
                     .withColumnRenamed("newSrc", "src")
                     .withColumnRenamed("newDst", "dst")
                     .select("src", "dst", "relationship", "cost"))

    relationships = rels.union(reversed_rels)

    return GraphFrame(nodes, relationships)
# // end::load-graph-frame[]

# // tag::load-graph-frame[]
def create_social_media_graph():
    v = spark.read.csv("finny-intelligence-engine/data/social-nodes.csv", header=True)
    e = spark.read.csv("finny-intelligence-engine/data/social-relationships.csv", header=True)
    return GraphFrame(v, e)
# // end::load-graph-frame[]

def create_software_graph():
    nodes = spark.read.csv("finny-intelligence-engine/data/sw-nodes.csv", header=True)
    relationships = spark.read.csv("finny-intelligence-engine/data/sw-relationships.csv", header=True)
    return GraphFrame(nodes, relationships)

def create_airport_graph():
    nodes = spark.read.csv("finny-intelligence-engine/data/airports.csv", header=False)
    cleaned_nodes = (nodes.select("_c1", "_c3", "_c4", "_c6", "_c7")
                     .filter("_c3 = 'United States'")
                     .withColumnRenamed("_c1", "name")
                     .withColumnRenamed("_c4", "id")
                     .withColumnRenamed("_c6", "latitude")
        .withColumnRenamed("_c7", "longitude")
        .drop("_c3"))
    cleaned_nodes = cleaned_nodes[cleaned_nodes["id"] != "\\N"]

    relationships = spark.read.csv("data/984660862_T_ONTIME_REPORTING.csv", header=True)
    cleaned_relationships = (relationships
                             .select("ORIGIN", "DEST", "FL_DATE", "DEP_DELAY", "ARR_DELAY",
                                     "DISTANCE", "TAIL_NUM", "OP_CARRIER_FL_NUM", "CRS_DEP_TIME",
                                     "CRS_ARR_TIME", "OP_UNIQUE_CARRIER")
                             .withColumnRenamed("ORIGIN", "src")
                             .withColumnRenamed("DEST", "dst")
                             .withColumnRenamed("DEP_DELAY", "deptDelay")
                             .withColumnRenamed("ARR_DELAY", "arrDelay")
                             .withColumnRenamed("TAIL_NUM", "tailNumber")
                             .withColumnRenamed("OP_CARRIER_FL_NUM", "flightNumber")
                             .withColumnRenamed("FL_DATE", "date")
                             .withColumnRenamed("CRS_DEP_TIME", "time")
                             .withColumnRenamed("CRS_ARR_TIME", "arrivalTime")
                             .withColumnRenamed("DISTANCE", "distance")
                             .withColumnRenamed("OP_UNIQUE_CARRIER", "airline")
                             .withColumn("deptDelay", F.col("deptDelay").cast(FloatType()))
                             .withColumn("arrDelay", F.col("arrDelay").cast(FloatType()))
                             .withColumn("time", F.col("time").cast(IntegerType()))
                             .withColumn("arrivalTime", F.col("arrivalTime").cast(IntegerType()))
                             )



    return GraphFrame(cleaned_nodes, cleaned_relationships)

def create_airlines_graph():
    airlines_reference = (spark.read.csv("finny-intelligence-engine/data/airlines.csv")
                          .select("_c1", "_c3")
                          .withColumnRenamed("_c1", "name")
                          .withColumnRenamed("_c3", "code"))

    airlines_reference = airlines_reference[airlines_reference["code"] != "null"]
    return airlines_reference