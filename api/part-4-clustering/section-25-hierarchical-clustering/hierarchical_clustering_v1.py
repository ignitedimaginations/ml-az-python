# Hierarchical Clustering

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Importing the mall dataset with pandas

dataset = pd.read_csv("../../../data/part-4-clustering/section-25-hierarchical-clustering/transaction-nodes.csv")
x = dataset.iloc[:, [1, 2, 3, 4, 5]].values

from sklearn.preprocessing import LabelEncoder
labelEncoder_x = LabelEncoder()
# Use fit_tranform to fit 1st column and return encoded values for the first column
x[:, 0] = labelEncoder_x.fit_transform(x[:, 0])
x
x[:, 1] = labelEncoder_x.fit_transform(x[:, 1])
x
x[:, 3] = labelEncoder_x.fit_transform(x[:, 3])
x

from sklearn.preprocessing import OneHotEncoder
oneHotEncoder = OneHotEncoder(categorical_features=[0, 1, 3])
x = oneHotEncoder.fit_transform(x).toarray()
x

from sklearn.preprocessing import StandardScaler
sc_x = StandardScaler()
x_train = sc_x.fit_transform(x)

# Using the dendrogram to find the optimal number of clusters
import scipy.cluster.hierarchy as sch
dendrogram = sch.dendrogram(sch.linkage(x_train, method='ward'))
plt.title('Transactions')
plt.ylabel('Euclidean distances')
plt.show()

# Fitting hierarchical clustering to the mall dataset
# Affinity = {“euclidean”, “l1”, “l2”, “manhattan”,“cosine”}
# Linkage = {“ward”, “complete”, “average”}
from sklearn.cluster import AgglomerativeClustering
hc = AgglomerativeClustering(n_clusters=2, affinity='euclidean', linkage='ward')
y_hc = hc.fit_predict(x_train)

# Visualising the clusters
#plt.scatter(x[1], y_hc, s=100, c='red', label='Careful')
# plt.scatter(x[y_hc == 1, 0], x[y_hc == 1, 1], s=100, c='blue', label='Standard')
#plt.scatter(x[y_hc == 2, 0], x[y_hc == 2, 1], s=100, c='green', label='Target')
#plt.scatter(x[y_hc == 3, 0], x[y_hc == 3, 1], s=100, c='cyan', label='Careless')
#plt.scatter(x[y_hc == 4, 0], x[y_hc == 4, 1], s=100, c='magenta', label='Sensible')
plt.title('Clusters of transactions')
plt.xlabel('Transactions X Axis')
plt.ylabel('Rule')
plt.legend()
plt.show()
