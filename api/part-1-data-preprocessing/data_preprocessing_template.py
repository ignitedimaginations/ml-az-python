# Data Preprocessing

# IMPORTING THE LIBRARIES
# Mathematical tools
import numpy as np
# To ploy diagrams
import matplotlib.pyplot as plt
# To import and manage datasets
import pandas as pd

# IMPORTING THE DATASET
from sklearn.impute import SimpleImputer

dataset = pd.read_csv("../../data/part-1-data-preprocessing/data.csv")
x = dataset.iloc[:, :-1].values
y = dataset.iloc[:, 3].values

# MISSING DATA
# Libraries to make ML models
# Imputer allows taking care of missing data
from sklearn.preprocessing import Imputer
# Replace Missing values with median of columns
# Assign NaN as missing value, use strategy as mean
# you can also use meadian or most_frequent strategy depending on the context
imputer = Imputer(missing_values='NaN', strategy="mean", axis=0)
# Use the independent array x and apply NaN in columns with index 1 and 2 using fit
# Note that index 1 is 1 bu index 2 is 3!
imputer = imputer.fit(x[:, 1:3])
# Use transform to update the NaN values with median of the columns with index 1 and 2
x[:, 1:3] = imputer.transform(x[:, 1:3])
# Let's check the dependent array now updated for missing values with median of columns
x

# ENCODING CATEGORICAL DATA
# In our data, two columns - Country and Purchased contain categorical data
# The values in Country column range between France, Spain and Germany
# The values in Purchased column are Yes or No
# We need to encode such categorical data as they are non numerical
from sklearn.preprocessing import LabelEncoder
labelEncoder_x = LabelEncoder()
# Use fit_tranform to fit 1st column and return encoded values for the first column
x[:, 0] = labelEncoder_x.fit_transform(x[:, 0])
# Check x after fit_transform and observe Country values as numerical encoded values
x
# Problem with ENCODING: Since the countries got assigned numerical values,
# the machine might assume that since Germany is assigned 0 and france is assigned 1, france > germany
# And that's incorrect assumption
# To present such interpretation, we need to created Dummy Encoding
# In Dummy encoding, we create 3 columns France, Germany and Spain
# And assign them numerical values in case of each Country value
from sklearn.preprocessing import OneHotEncoder
oneHotEncoder = OneHotEncoder(categorical_features=[0])
x = oneHotEncoder.fit_transform(x).toarray()
# Check x after fit_transform and observe Country values with dummy encoding in 3 columns for country alone
x
# For Purchased column, we will just use labelEncoder as it's a dependent variable
# No dummy encoding needed
labelEncoder_y = LabelEncoder()
y = labelEncoder_y.fit_transform(y)
# Let's check dependent variable Purchased values with encoding
y

# SPLITTING THE DATASET INTO TRAINING SET AND TEST SET
from sklearn.model_selection import train_test_split
# Dividing data set into 80% Training and 20% Test data, set test_size = 0.2, set random state as 0 for no randomness
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=0)
# Let's check the training sets and test sets
x_train, x_test, y_train, y_test

# FEATURE SCALING
# In the given dataset, we have numerical values for age and salary
# Since the scale of salary is much higher than age, in ML equations, using it directly will give a bias to salary
# In order to given age it's due importance we need to scale different variables
# Scaling can be done using methods like Standardisation and Normalisation
from sklearn.preprocessing import StandardScaler
sc_x = StandardScaler()
x_train = sc_x.fit_transform(x_train)
x_test = sc_x.fit_transform(x_test)
# Should we scale dummy variables? You can. It depends on the context. It wont break your model if we don't scale.
# Let's check the scaled versions of x train and x test
# No need to apply feature scaling for y as it's a 1d array and there is no 2nd column to compare and scale with
# In our case y contains 1 and 0 and it's more of classification
x_test, x_test
