# Data Preprocessing

# IMPORTING THE LIBRARIES
# Mathematical tools
import numpy as np
# To ploy diagrams
import matplotlib.pyplot as plt
# To import and manage datasets
import pandas as pd

# IMPORTING THE DATASET
from sklearn.impute import SimpleImputer

dataset = pd.read_csv("../../data/part-1-data-preprocessing/data.csv")
x = dataset.iloc[:, :-1].values
y = dataset.iloc[:, 3].values


# SPLITTING THE DATASET INTO TRAINING SET AND TEST SET
from sklearn.model_selection import train_test_split

# Dividing data set into 80% Training and 20% Test data, set test_size = 0.2, set random state as 0 for no randomness
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=0)
# Let's check the training sets and test sets
x_train, x_test, y_train, y_test

# FEATURE SCALING
# In the given dataset, we have numerical values for age and salary
# Since the scale of salary is much higher than age, in ML equations, using it directly will give a bias to salary
# In order to given age it's due importance we need to scale different variables
# Scaling can be done using methods like Standardisation and Normalisation
# COMMENTED as most of the ML libraries have in-built feature scaling
"""from sklearn.preprocessing import StandardScaler
sc_x = StandardScaler()
x_train = sc_x.fit_transform(x_train)
x_test = sc_x.fit_transform(x_test)
# Should we scale dummy variables? You can. It depends on the context. Iet wont break your model if we don't scale.
# Let's check the scaled versions of x train and x test
# No need to apply feature scaling for y as it's a 1d array and there is no 2nd column to compare and scale with
# In our case y contains 1 and 0 and it's more of classification
x_test, x_test"""
