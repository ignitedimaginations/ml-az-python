# Artificial Neural Network

# Installing Theano
# pip3 install --upgrade --no-deps git+git://github.com/Theano/Theano.git

# Installing Tensorflow
# Install Tensorflow from the website : https://www.tensorflow.org/install

# Install Keras
# pip install --upgrade keras

# To import and manage datasets
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import tensorflow as tf

# Part 1 - Data preprocessing

# IMPORTING THE DATASET


dataset = pd.read_csv("../../../data/part-8-deep-learning/section-31-artificial-neural-networks/Churn_Modelling.csv")
# We just want to include Age and Estimated Salary as the independent variables
x = dataset.iloc[:, 3:13].values
y = dataset.iloc[:, 13].values

# Encoding categorical data
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
labelEncoder_X_1 = LabelEncoder()
x[:, 1] = labelEncoder_X_1.fit_transform(x[:, 1])
labelEncoder_X_2 = LabelEncoder()
x[:, 2] = labelEncoder_X_2.fit_transform(x[:, 2])
oneHotEncoder = OneHotEncoder(categorical_features=[1])
x = oneHotEncoder.fit_transform(x).toarray()
# Remove one dummy variable to avoid falling into dummy variable trap
x = x[:, 1:]

# SPLITTING THE DATASET INTO TRAINING SET AND TEST SET
from sklearn.model_selection import train_test_split
# Dividing data set into 80% Training and 20% Test data, set test_size = 0.2, set random state as 0 for no randomness
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=0)
# Let's check the training sets and test sets
x_train, x_test, y_train, y_test

# FEATURE SCALING
# In the given dataset, we have numerical values for age and salary
# Since the scale of salary is much higher than age, in ML equations, using it directly will give a bias to salary
# In order to given age it's due importance we need to scale different variables
# Scaling can be done using methods like Standardisation and Normalisation
# COMMENTED as most of the ML libraries have in-built feature scaling
from sklearn.preprocessing import StandardScaler
sc_x = StandardScaler()
x_train = sc_x.fit_transform(x_train)
x_test = sc_x.fit_transform(x_test)

# Part 2 - Let's make ANN
# Importing the Keras libraries and packages
import keras
from keras.models import Sequential
from keras.layers import Dense

# Initializing the ANN
# Defining it as a sequence of layers
classifier = Sequential()

# Choosing rectifier function (relu) for hidden layer and sigmoid function for output layer
# Adding input layer and first hidden layer, activation function used as relu for rectifier function
classifier.add(Dense(output_dim=6, init='uniform', activation='relu', input_dim=11))

# Adding second hidden layer, no need for input dim as we already have the first hidden layer
classifier.add(Dense(output_dim=6, init='uniform', activation='relu'))

# Adding output layer, now we need sigmoid activation function
classifier.add(Dense(output_dim=1, init='uniform', activation='sigmoid'))

# Compiling the ANN
# Using adam algorithm for stochastic gradient descent, Logarithmic loss function binary cross entropy
classifier.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

# Fitting the ANN to the training set
classifier.fit(x_train, y_train, batch_size=10, nb_epoch=100)

# Part 3 - making the predictions and evaluation the model

# Predicting the Test set results
# Returns the probabilities that the customers leave the bank
y_pred = classifier.predict(x_test)
# Probability more than 50% means that customer will leave
y_pred = (y_pred > 0.5)

# Making the confusion matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)